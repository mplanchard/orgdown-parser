// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//! Lex an org file into tokens

use std::{iter::Peekable, str::CharIndices};

use itertools::Itertools;

use crate::tokens::{CheckboxState, Indent, NewLine, UnorderedListBullet};

use super::tokens::{Block, Token};

#[derive(Debug, Copy, Clone, PartialEq)]
struct Position {
    line: u64,
    // TODO: column won't necessarily be perfectly accurate from a user perspective
    // when dealing with graphmeme clusters. Is this okay? Seems fine, since e.g.
    // emacs treats column as index rather than graphememe cluster.
    column: u64,
}
impl Position {
    fn new() -> Self {
        Self { line: 0, column: 0 }
    }

    fn at(line: u64, column: u64) -> Self {
        Self { line, column }
    }

    fn advance_line(&mut self) {
        self.column = 0;
        self.line += 1;
    }

    fn advance_column(&mut self) {
        self.column += 1;
    }
}

#[derive(Debug, PartialEq)]
struct LexedToken<'input> {
    token: Token<'input>,
    position: Position,
}

impl<'input> LexedToken<'input> {
    fn new(token: Token<'input>, position: Position) -> Self {
        Self { token, position }
    }
}

struct InnerScan {
    start: usize,
    offset: usize,
}

struct Scanner<'input> {
    position: Position,
    absolute_position: usize,
    found_line_content: bool,
    // inner: Option<InnerScan>,
    input: &'input str,
}
impl<'input> Scanner<'input> {
    fn new(input: &'input str) -> Self {
        Self {
            input,
            absolute_position: 0,
            found_line_content: false,
            position: Position::new(),
        }
    }

    fn peek_n(&self, n: usize) -> Option<&'input str> {
        self.input
            .get(self.absolute_position..self.absolute_position + n)
    }

    fn peek(&self) -> Option<&'input str> {
        self.input
            .get(self.absolute_position..self.absolute_position + 1)
    }

    fn found_line_content(&mut self) {
        if !self.found_line_content {
            self.found_line_content = true;
        };
    }

    fn reset_position(&mut self, start_idx: usize, position: Position) {
        self.absolute_position = start_idx;
        self.position = position;
    }

    fn advance_line(&mut self) {
        self.position.advance_line();
        self.found_line_content = false;
    }

    fn advance_column(&mut self) {
        self.absolute_position += 1;
        self.position.advance_column();
    }
}
impl<'input> Iterator for Scanner<'input> {
    type Item = LexedToken<'input>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut end_offset: usize;
        let current = self.peek()?;
        let position = self.position;
        let start_idx = self.absolute_position;
        self.advance_column();
        // self.absolute_position += 1;
        // self.position.advance_column();

        let token_at_start = |t: Token<'input>| LexedToken::new(t, position);

        let token = match current {
            // "*" => {
            //     if !self.found_line_content {}
            // },
            "-" => {
                // match for dash things that are only valid before any other
                // content in the line.
                if !self.found_line_content {
                    // don't bother with the rest of the matching if it doesn't
                    // look like we might be a checkbox
                    if let Some(" [") = self.peek_n(2) {
                        match self.peek_n(5) {
                            Some(" [x] ") => {
                                return Some(token_at_start(Token::Checkbox(
                                    CheckboxState::Checked,
                                )))
                            }
                            Some(" [X] ") => {
                                return Some(token_at_start(Token::Checkbox(
                                    CheckboxState::Checked,
                                )))
                            }
                            Some(" [ ] ") => {
                                return Some(token_at_start(Token::Checkbox(CheckboxState::Empty)))
                            }
                            Some(" [-] ") => {
                                return Some(token_at_start(Token::Checkbox(
                                    CheckboxState::Undecided,
                                )))
                            }
                            _ => {}
                        }
                    }

                    if let Some(" ") = self.peek() {
                        return Some(token_at_start(Token::UnorderedListItemPrelude(
                            UnorderedListBullet::Dash,
                        )));
                    }

                    if let Some("----") = self.peek_n(4) {
                        let mut num_dashes = 1;
                        while let Some("-") = self.peek() {
                            self.advance_column();
                            num_dashes += 1;
                        }
                        // if it is followed by anything other than whitespace,
                        // it isn't a horizontal line
                        match self.peek() {
                            Some(" " | "\t" | "\n") | None => {
                                return Some(token_at_start(Token::HorizontalRule { num_dashes }))
                            }
                            _ => {
                                self.reset_position(start_idx, position);
                            }
                        }
                    }
                }

                // Sometimes a dash is just a dash.
                Some(token_at_start(Token::Dash))
            }
            "+" => {
                // if we're at the start of the line, it could be a part of an
                // ordered list
                if !self.found_line_content {
                    match self.peek() {
                        Some(" ") => {
                            return Some(token_at_start(Token::UnorderedListItemPrelude(
                                UnorderedListBullet::Plus,
                            )))
                        }
                        _ => {}
                    }
                }
                Some(token_at_start(Token::Plus))
            }
            "|" => Some(token_at_start(Token::Pipe)),
            "\n" => {
                self.advance_line();
                Some(token_at_start(Token::Newline))
            }
            "[" => match self.peek() {
                Some("[") => {
                    self.advance_column();
                    Some(token_at_start(Token::LinkStart))
                }
                _ => Some(token_at_start(Token::LeftBracket)),
            },
            "]" => match self.peek() {
                Some("]") => {
                    self.advance_column();
                    Some(token_at_start(Token::LinkEnd))
                }
                _ => Some(token_at_start(Token::RightBracket)),
            },
            v @ (" " | "\t") => {
                if position.column != 0 {
                    return None;
                }
                let mut effective_spaces = match v {
                    " " => 1,
                    "\t" => 4,
                    _ => 0,
                };

                while let Some(v @ (" " | "\t")) = self.peek() {
                    self.advance_column();
                    effective_spaces += match v {
                        " " => 1,
                        "\t" => 4,
                        _ => 0,
                    };
                }

                Some(token_at_start(Token::Indent {
                    text: &self.input[start_idx..self.absolute_position],
                    effective_spaces,
                }))
            }
            _ => None,
        };

        // if we found a token other than Indent or Newline, mark ourselves
        // as having found some line content in the current line, so we can
        // track whether something is happening at the start (minus indentation)
        // of a line.
        if let Some(ref lexed_token) = token {
            match lexed_token.token {
                Token::Indent { .. } | Token::Newline => {}
                _ => {
                    self.found_line_content();
                }
            }
        };

        token
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn lex_plus() {
        assert_eq!(
            Scanner::new("+").next(),
            Some(LexedToken::new(Token::Plus, Position::new()))
        );
    }

    #[test]
    fn lex_dash() {
        assert_eq!(
            Scanner::new("-").next(),
            Some(LexedToken::new(Token::Dash, Position::new()))
        );
    }

    #[test]
    fn lex_unordered_list_dash() {
        assert_eq!(
            Scanner::new("- ").next(),
            Some(LexedToken::new(
                Token::UnorderedListItemPrelude(UnorderedListBullet::Dash),
                Position::new()
            ))
        );
    }

    #[test]
    fn lex_unordered_list_plus() {
        assert_eq!(
            Scanner::new("+ ").next(),
            Some(LexedToken::new(
                Token::UnorderedListItemPrelude(UnorderedListBullet::Plus),
                Position::new()
            ))
        );
    }

    #[test]
    fn lex_dashes() {
        let cases = [
            ("-", vec![Token::Dash]),
            ("--", vec![Token::Dash, Token::Dash]),
            ("---", vec![Token::Dash, Token::Dash, Token::Dash]),
            (
                "----",
                vec![Token::Dash, Token::Dash, Token::Dash, Token::Dash],
            ),
        ];

        cases.into_iter().for_each(|(text, tokens)| {
            assert_eq!(
                Scanner::new(text).collect::<Vec<_>>(),
                tokens
                    .into_iter()
                    .enumerate()
                    .map(|(idx, t)| LexedToken::new(t, Position::at(0, idx as u64)))
                    .collect::<Vec<_>>()
            )
        });
    }

    #[test]
    fn lex_checkboxes() {
        let cases = [
            ("- [ ] ", vec![Token::Checkbox(CheckboxState::Empty)]),
            ("- [X] ", vec![Token::Checkbox(CheckboxState::Checked)]),
            ("- [x] ", vec![Token::Checkbox(CheckboxState::Checked)]),
            ("- [-] ", vec![Token::Checkbox(CheckboxState::Undecided)]),
        ];

        cases.into_iter().for_each(|(text, tokens)| {
            assert_eq!(
                Scanner::new(text).collect::<Vec<_>>(),
                tokens
                    .into_iter()
                    .enumerate()
                    .map(|(idx, t)| LexedToken::new(t, Position::at(0, idx as u64)))
                    .collect::<Vec<_>>()
            )
        });
    }

    #[test]
    fn lex_horizontal_rule() {
        let cases = [
            ("-----", Token::HorizontalRule { num_dashes: 5 }),
            ("------", Token::HorizontalRule { num_dashes: 6 }),
            ("-------", Token::HorizontalRule { num_dashes: 7 }),
        ];

        cases.into_iter().for_each(|(text, token)| {
            assert_eq!(
                Scanner::new(text).next(),
                Some(LexedToken::new(token, Position::new()))
            )
        });
    }

    #[test]
    fn lex_pipe() {
        assert_eq!(
            Scanner::new("|").next(),
            Some(LexedToken::new(Token::Pipe, Position::new()))
        );
    }

    #[test]
    fn lex_newline() {
        assert_eq!(
            Scanner::new("\n").next(),
            Some(LexedToken::new(Token::Newline, Position::new()))
        );
    }

    #[test]
    fn lex_left_bracket() {
        assert_eq!(
            Scanner::new("[").next(),
            Some(LexedToken::new(Token::LeftBracket, Position::new()))
        );
    }

    #[test]
    fn lex_right_bracket() {
        assert_eq!(
            Scanner::new("]").next(),
            Some(LexedToken::new(Token::RightBracket, Position::new()))
        );
    }

    #[test]
    fn lex_link_start() {
        assert_eq!(
            Scanner::new("[[").next(),
            Some(LexedToken::new(Token::LinkStart, Position::new()))
        );
    }

    #[test]
    fn lex_link_end() {
        assert_eq!(
            Scanner::new("]]").next(),
            Some(LexedToken::new(Token::LinkEnd, Position::new()))
        );
    }

    #[test]
    fn lex_indent() {
        let cases = [
            (
                " ",
                LexedToken::new(
                    Token::Indent {
                        text: " ",
                        effective_spaces: 1,
                    },
                    Position::new(),
                ),
            ),
            (
                "\t",
                LexedToken::new(
                    Token::Indent {
                        text: "\t",
                        effective_spaces: 4,
                    },
                    Position::new(),
                ),
            ),
            (
                "  \t",
                LexedToken::new(
                    Token::Indent {
                        text: "  \t",
                        effective_spaces: 6,
                    },
                    Position::new(),
                ),
            ),
            (
                "  \t\n",
                LexedToken::new(
                    Token::Indent {
                        text: "  \t",
                        effective_spaces: 6,
                    },
                    Position::new(),
                ),
            ),
            (
                "  \t\n",
                LexedToken::new(
                    Token::Indent {
                        text: "  \t",
                        effective_spaces: 6,
                    },
                    Position::new(),
                ),
            ),
        ];

        cases
            .into_iter()
            .for_each(|(text, token)| assert_eq!(Scanner::new(text).next(), Some(token),))
    }
}

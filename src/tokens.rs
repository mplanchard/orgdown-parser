// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//! Tokens that may be present in an org file

use itertools::Itertools;
use once_cell::sync::Lazy;
use regex::Regex;

#[derive(Debug, PartialEq)]
pub enum EnumeratedListOrdinal {
    Number(u64),
    Letter(char),
}

#[derive(Debug, PartialEq)]
pub enum EnumeratedListDelimiter {
    Dot,
    RightParen,
}

#[derive(Debug, PartialEq)]
pub enum UnorderedListBullet {
    Dash,
    Plus,
    Asterisk,
}

#[derive(Default)]
pub struct TextStyle {
    bold: bool,
    italic: bool,
    undeline: bool,
    strikethrough: bool,
}

pub struct LiteralText(String);

pub enum StyledText {
    Literal(LiteralText),
    Styled { text: String, style: TextStyle },
    Code { text: String, style: TextStyle },
    Commands { text: String, style: TextStyle },
}

#[derive(Debug, PartialEq)]
pub enum CheckboxState {
    Empty,
    Checked,
    Undecided,
}

#[derive(Debug, PartialEq)]
pub struct HeadingPrelude(u32);

#[derive(Debug, PartialEq)]
pub struct Indent<'input> {
    text: &'input str,
    effective_spaces: u32,
}

#[derive(Debug, PartialEq)]
pub struct BlockStart(Block);

#[derive(Debug, PartialEq)]
pub struct BlockEnd(Block);

#[derive(Debug, PartialEq)]
pub struct EnumeratedListItemPrelude {
    ordinal: EnumeratedListOrdinal,
    delimiter: EnumeratedListDelimiter,
}

#[derive(Debug, PartialEq)]
pub struct ExamplePrelude;

#[derive(Debug, PartialEq)]
pub struct CommentPrelude;

#[derive(Debug, PartialEq)]
pub struct LinkStart;

#[derive(Debug, PartialEq)]
pub struct LinkEnd;

#[derive(Debug, PartialEq)]
pub struct LeftBracket;

#[derive(Debug, PartialEq)]
pub struct RightBracket;

#[derive(Debug, PartialEq)]
pub struct HorizontalRule<'input> {
    num_dashes: u32,
    preceding_whitespace: Indent<'input>,
}

#[derive(Debug, PartialEq)]
pub struct NewLine;

#[derive(Debug, PartialEq)]
pub struct Dash;

#[derive(Debug, PartialEq)]
pub struct Pipe;

#[derive(Debug, PartialEq)]
pub struct PlusSign;

#[derive(Debug, PartialEq)]
pub struct Slash;

#[derive(Debug, PartialEq)]
pub struct StyledTextStart<'input>(&'input str);

#[derive(Debug, PartialEq)]
pub struct StyledTextEnd<'input>(&'input str);

#[derive(Debug, PartialEq)]
pub struct Word<'input>(&'input str);

#[derive(Debug, PartialEq)]
pub struct UnorderedListItemPrelude(UnorderedListBullet);

#[derive(Debug, PartialEq)]
pub enum Token<'input> {
    HeadingPrelude(HeadingPrelude),
    BlockStart(BlockStart),
    BlockEnd(BlockEnd),
    Checkbox(CheckboxState),
    EnumeratedListItemPrelude(EnumeratedListItemPrelude),
    ExamplePrelude(ExamplePrelude),
    CommentPrelude(CommentPrelude),
    LinkStart,
    LinkEnd,
    LeftBracket,
    RightBracket,
    HorizontalRule {
        num_dashes: u32,
    },
    Indent {
        text: &'input str,
        effective_spaces: u32,
    },
    Newline,
    Dash,
    Pipe,
    Plus,
    StyledTextStart(StyledTextStart<'input>),
    StyledTextEnd(StyledTextEnd<'input>),
    Word(Word<'input>),
    UnorderedListItemPrelude(UnorderedListBullet),
}

#[derive(Debug, PartialEq)]
pub enum Block {
    Comment,
    Example,
    Quote,
    Verse,
    Source,
}

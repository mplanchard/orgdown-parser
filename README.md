# orddown-parser (WIP)

Working on a parser for the [orgdown](https://gitlab.com/publicvoit/orgdown)
specification. Not for public consumption at this time.

## Setup

If you have nix, rust and all other dependencies can be installed in an isolated 
environment just for this project.

- Using direnv and nix:

  ``` sh
  direnv allow
  ```

- Using nix (with flakes):

  ``` sh
  nix develop
  ```

- Using nix (no flakes):

  ``` sh
  nix-shell
  ```

- Other:
  - Install `rustup`
  - `rustup show && rustup update`
